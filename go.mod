module gitlab.com/aubrey.s.lavigne/roller-derby-api

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/spf13/pflag v1.0.3
)
