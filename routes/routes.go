package routes

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/aubrey.s.lavigne/roller-derby-api/models"
)

func writeOutput(w http.ResponseWriter, val interface{}) {
	// Format output
	output, err := json.Marshal(val)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Could not create JSON response")
		return
	}

	// Respond with correct data
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(output))
}

func intVar(r *http.Request, name string) (int, error) {
	// Parse parameters
	vars := mux.Vars(r)
	teamIDstr := vars["teamid"]
	return strconv.Atoi(teamIDstr)
}

// TeamLookupRoute responds with the team represented by teamid
func TeamLookupRoute(w http.ResponseWriter, r *http.Request) {
	// Parse parameters
	teamID, err := intVar(r, "teamid")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid 'teamid' parameter")
		return
	}

	// Establish database
	conn, err := sql.Open("sqlite3", "fts.db")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	// Load team
	var team models.Team
	err = team.LoadID(conn, teamID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	writeOutput(w, team)
}

// AllDomainsRoute responds with a list of all avilable domains
func AllDomainsRoute(w http.ResponseWriter, r *http.Request) {
	// Establish Database
	conn, err := sql.Open("sqlite3", "fts.db")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	// Load domains
	domains, err := models.LoadAllDomains(conn)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	writeOutput(w, domains)
}

// AllGeneraRoute responds with a list of all avilable genera
func AllGeneraRoute(w http.ResponseWriter, r *http.Request) {
	// Establish Database
	conn, err := sql.Open("sqlite3", "fts.db")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	// Load domains
	genera, err := models.LoadAllGenera(conn)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	writeOutput(w, genera)
}

// AllTeamTypesRoute responds with a list of all avilable team types
func AllTeamTypesRoute(w http.ResponseWriter, r *http.Request) {
	// Establish Database
	conn, err := sql.Open("sqlite3", "fts.db")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	// Load domains
	teamTypes, err := models.LoadAllTeamTypes(conn)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error reading from database")
		return
	}

	// Format output
	writeOutput(w, teamTypes)
}

// CatchallHandler responds with a "501 - Not Implemented" code as a fallback
// for any request without a corresponding route
func CatchallHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
	fmt.Fprintf(w, "Invalid Request -- No Route found")
}
