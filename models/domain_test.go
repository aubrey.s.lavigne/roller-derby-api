package models

import (
	"database/sql"
	"testing"
)

func TestInsertAndLoadDomain(t *testing.T) {
	conn, err := sql.Open("sqlite3", ":memory:?_foreign_keys=on")
	defer conn.Close()
	if err != nil {
		t.Fatalf("Could not open in-memory Sqlite3 connection: %q", err)
	}

	// Set up test database
	err = createSchema(conn)
	if err != nil {
		t.Errorf("Could not create database schema: %q", err)
	}

	// Check that the domain starts empty
	emptyDomain, err := loadDomainByName(conn, "Europe")
	if emptyDomain.Found {
		t.Errorf("Empty domain should not be found")
	}

	// Insert a single domain
	savingDomain := Domain{
		Name: "Europe",
	}
	err = savingDomain.save(conn)
	if err != nil {
		t.Fatalf("Should not error while saving domain")
	}

	loadedDomain, err := loadDomainByName(conn, "Europe")
	if !loadedDomain.Found {
		t.Errorf("Loaded Domain should be found")
	}
	if id := loadedDomain.ID; id != 1 {
		t.Errorf("Unexpected loaded domain ID. Got %d, expected %d", id, 1)
	}
	if name := loadedDomain.Name; name != "Europe" {
		t.Errorf("Unexpected loaded domain name. Got %s, expected %s", name, "Europe")
	}
}
