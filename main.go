package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/aubrey.s.lavigne/roller-derby-api/models"
	"gitlab.com/aubrey.s.lavigne/roller-derby-api/routes"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	flags "github.com/spf13/pflag"
)

var teamFilename *string = flags.String("import-team-file", "", "Name of team snapshot to import")

func main() {

	flags.Parse()

	log.Printf("Starting init...")

	conn, err := sql.Open("sqlite3", "fts.db")
	if err != nil {
		log.Fatalf("Error reading from database")
		return
	}

	err = models.MigrateDB(conn)
	if err != nil {
		log.Fatalf("%v", err)
		return
	}

	if *teamFilename != "" {
		fin, err := os.Open(*teamFilename)
		if err != nil {
			log.Fatalf("Could not open provided file '%s': %q", *teamFilename, err)
		}

		err = models.ImportTeamCSV(conn, fin)
		if err != nil {
			log.Fatalf("Import Failed: %q", err)
			return
		}
	}

	routes := []struct {
		route   string
		handler func(w http.ResponseWriter, r *http.Request)
		method  string
	}{
		{route: "/team/{teamid:[0-9]+}", handler: routes.TeamLookupRoute, method: "GET"},
		{route: "/domains", handler: routes.AllDomainsRoute, method: "GET"},
		{route: "/genera", handler: routes.AllGeneraRoute, method: "GET"},
		{route: "/team-types", handler: routes.AllTeamTypesRoute, method: "GET"},
		{route: "/", handler: routes.CatchallHandler, method: "GET"},
	}

	r := mux.NewRouter()
	for _, route := range routes {
		r.HandleFunc(route.route, route.handler).
			Methods(route.method)
	}

	portNum := 8888
	log.Printf("Listening on Port %d...", portNum)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", portNum), r))
}
