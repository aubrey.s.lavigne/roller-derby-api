package models

import (
	"database/sql"

	"gitlab.com/aubrey.s.lavigne/roller-derby-api/db"
)

// TeamType repesents possible team levels
type TeamType struct {
	Found bool `json:"-"`

	ID   int    `json:"id" field:"id,primarykey"`
	Name string `json:"name" field:"name"`
}

func (tt *TeamType) save(conn *sql.DB) error {
	return db.Save(tt, conn)
}

// SetID sets the ID property for the Domain
func (tt *TeamType) SetID(id int64) {
	tt.ID = int(id)
}

// Insert inserts the TeamType into the team_types table
func (tt *TeamType) Insert(conn *sql.DB) (sql.Result, error) {
	return insertTeamType(conn, *tt)
}

func createTeamTypeSchema(conn *sql.DB) error {
	stmt, err := conn.Prepare(`
    CREATE TABLE IF NOT EXISTS team_types
    (
       id   INTEGER PRIMARY KEY,
       name TEXT    NOT NULL
    )
    `)
	defer stmt.Close()
	if err != nil {
		return err
	}

	_, err = stmt.Exec()
	return err
}

func seedTeamType(conn *sql.DB) error {
	teamTypes := []string{
		"B Team",
		"Exhibition Team",
		"Home Team",
		"Travel Team",
	}

	for _, teamType := range teamTypes {
		obj, err := loadTeamTypeByName(conn, teamType)
		if err != nil {
			return err
		}

		if !obj.Found {
			obj.Name = teamType
			err = obj.save(conn)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func loadTeamTypeByName(conn *sql.DB, teamTypeName string) (TeamType, error) {
	var res TeamType

	err := conn.QueryRow(`
    SELECT id, name
    FROM team_types
    WHERE name = ?
    `, teamTypeName).Scan(&res.ID, &res.Name)

	res.Found = (err == nil)
	if err == sql.ErrNoRows {
		err = nil
	}

	return res, err
}

func insertTeamType(conn *sql.DB, teamType TeamType) (sql.Result, error) {
	stmt, err := conn.Prepare(`
    INSERT INTO team_types (name)
    VALUES (?)
    `)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	return stmt.Exec(teamType.Name)
}

// LoadAllTeamTypes returns a slice of all available TeamTypes
func LoadAllTeamTypes(conn *sql.DB) ([]TeamType, error) {
	res := []TeamType{}

	rows, err := conn.Query(`
    SELECT id, name
    FROM team_types
    `)
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var d TeamType
		err := rows.Scan(&d.ID, &d.Name)
		if err != nil {
			return res, err
		}
		res = append(res, d)
	}

	return res, err
}

func teamTypesLookupMap(conn *sql.DB) (map[string]int, error) {
	return db.TableLookupMap(conn, "team_types", "id", "name")
}
