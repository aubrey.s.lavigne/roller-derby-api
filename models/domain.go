package models

import (
	"database/sql"

	"gitlab.com/aubrey.s.lavigne/roller-derby-api/db"
)

// Domain covers possible geographic regions
type Domain struct {
	Found bool `json:"-"`

	ID   int    `json:"id" field:"id,primarykey"`
	Name string `json:"name" field:"name"`
}

func (d *Domain) save(conn *sql.DB) error {
	return db.Save(d, conn)
}

// SetID sets the ID property for the Domain
func (d *Domain) SetID(id int64) {
	d.ID = int(id)
}

// Insert inserts the Domain into the domains table
func (d *Domain) Insert(conn *sql.DB) (sql.Result, error) {
	return insertDomain(conn, *d)
}

func createDomainSchema(conn *sql.DB) error {
	stmt, err := conn.Prepare(`
    CREATE TABLE IF NOT EXISTS domains
    (
       id   INTEGER PRIMARY KEY,
       name TEXT    NOT NULL
    )
    `)
	defer stmt.Close()
	if err != nil {
		return err
	}

	_, err = stmt.Exec()
	return err
}

func seedDomain(conn *sql.DB) error {
	domains := []string{
		"Australia",
		"Canada",
		"Europe",
		"New Zealand",
		"Pacific",
		"USA",
	}

	for _, domain := range domains {

		obj, err := loadDomainByName(conn, domain)
		if err != nil {
			return err
		}

		if !obj.Found {
			obj.Name = domain
			err = obj.save(conn)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func loadDomainByName(conn *sql.DB, domainName string) (Domain, error) {
	var res Domain

	err := conn.QueryRow(`
    SELECT id, name
    FROM domains
    WHERE name = ?
    `, domainName).Scan(&res.ID, &res.Name)

	res.Found = (err == nil)
	if err == sql.ErrNoRows {
		err = nil
	}

	return res, err
}

// LoadAllDomains returns a lice containing all domain values
func LoadAllDomains(conn *sql.DB) ([]Domain, error) {
	res := []Domain{}

	rows, err := conn.Query(`
    SELECT id, name
    FROM domains
    `)
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var d Domain
		err := rows.Scan(&d.ID, &d.Name)
		if err != nil {
			return res, err
		}
		res = append(res, d)
	}

	return res, err
}

func insertDomain(conn *sql.DB, domain Domain) (sql.Result, error) {
	stmt, err := conn.Prepare(`
    INSERT INTO domains (name)
    VALUES (?)
    `)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	return stmt.Exec(domain.Name)
}

func domainsLookupMap(conn *sql.DB) (map[string]int, error) {
	return db.TableLookupMap(conn, "domains", "id", "name")
}
