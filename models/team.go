package models

import (
	"database/sql"
	"encoding/csv"
	"io"
	"strconv"
	"strings"
)

// Team represents a single Roller Derby team
type Team struct {
	Found bool `json:"-"`

	ID              int       `json:"id"`
	Name            string    `json:"name"`
	ShortName       string    `json:"short_name"`
	Abbreviation    string    `json:"abbreviation"`
	Website         string    `json:"website"`
	TeamType        *TeamType `json:"team_type,omitempty"`
	Location        string    `json:"location"`
	ParentLeague    *Team     `json:"parent_league,omitempty"`
	EstablishedDate string    `json:"established_date"`
	DisbandedDate   string    `json:"disbanded_date"`
	Genus           *Genus    `json:"genus,omitempty"`
	Domain          *Domain   `json:"domain,omitempty"`
}

func (t *Team) save(conn *sql.DB) error {
	res, err := insertTeam(conn, *t)
	if err != nil {
		return err
	}

	id64, err := res.LastInsertId()
	t.ID = int(id64)
	return err
}

// LoadID queries the database to populate the Team t
func (t *Team) LoadID(conn *sql.DB, ID int) error {
	var teamTypeID sql.NullInt64
	var teamTypeName sql.NullString
	var parentLeagueID sql.NullInt64
	var genusID sql.NullInt64
	var genusName sql.NullString
	var domainID sql.NullInt64
	var domainName sql.NullString

	var establishedDate string
	var disbandedDate string

	t.ID = ID

	err := conn.QueryRow(`
        SELECT 
            teams.name,
            teams.short_name,
            teams.abbreviation,
            teams.website,
            teams.established_date,
            teams.disbanded_date,

            teams.team_type_id,
            team_types.name AS team_type_name,
            teams.location,
            teams.parent_league_id,
            teams.genus_id,
            genera.name AS genus_name,
            teams.domain_id,
            domains.name AS domain_name
        FROM teams
            LEFT JOIN team_types ON team_types.id = teams.team_type_id
            LEFT JOIN genera ON genera.id = teams.genus_id
            LEFT JOIN domains ON domains.id = teams.domain_id
        WHERE teams.id = ?
    `, t.ID).
		Scan(&t.Name,
			&t.ShortName,
			&t.Abbreviation,
			&t.Website,
			&establishedDate,
			&disbandedDate,

			&teamTypeID,
			&teamTypeName,
			&t.Location,
			&parentLeagueID,
			&genusID,
			&genusName,
			&domainID,
			&domainName)

	if err != nil {
		t.Found = false
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}

	if teamTypeID.Valid {
		t.TeamType = &TeamType{
			ID:   int(teamTypeID.Int64),
			Name: teamTypeName.String,
		}
	}
	if parentLeagueID.Valid {
		t.ParentLeague = &Team{ID: int(parentLeagueID.Int64)}
	}
	if genusID.Valid {
		t.Genus = &Genus{
			ID:   int(genusID.Int64),
			Name: genusName.String,
		}
	}
	if domainID.Valid {
		t.Domain = &Domain{
			ID:   int(domainID.Int64),
			Name: domainName.String,
		}
	}

	t.EstablishedDate = establishedDate
	t.DisbandedDate = disbandedDate

	t.Found = true

	return err
}

// ImportTeamCSV imports the provided io.Reader
//
// The provided io.Reader expects data to be in CSV-format. It's also
// expected to match the snapshot format exported by Flat Track Stats
func ImportTeamCSV(conn *sql.DB, fileIn io.Reader) error {
	csvIn := csv.NewReader(fileIn)

	teamTypes, err := teamTypesLookupMap(conn)
	if err != nil {
		return err
	}

	domains, err := domainsLookupMap(conn)
	if err != nil {
		return err
	}

	genera, err := generaLookupMap(conn)
	if err != nil {
		return err
	}

	for {
		row, err := csvIn.Read()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		// Ignore header
		if row[0] == "id" {
			continue
		}

		// ID
		id, err := strconv.Atoi(row[0])
		if err != nil {
			return err
		}

		// Check whether the team exists
		var team Team
		err = team.LoadID(conn, id)
		if err != nil {
			return err
		}

		if team.Found {
			continue
		}

		// The team doesn't currently exist, so let's add it
		team.ID = id
		team.Name = strings.TrimSpace(row[1])
		team.ShortName = strings.TrimSpace(row[2])
		team.Abbreviation = row[3]
		team.Location = row[5]
		team.Website = row[6]

		if row[4] != "" {
			if teamTypeID, ok := teamTypes[row[4]]; ok {
				team.TeamType = &TeamType{
					ID: teamTypeID,
				}
			}
		}

		if row[10] != "" {
			if genusID, ok := genera[row[10]]; ok {
				team.Genus = &Genus{
					ID: genusID,
				}
			}
		}

		if row[11] != "" {
			if domainID, ok := domains[row[11]]; ok {
				team.Domain = &Domain{
					ID: domainID,
				}
			}
		}

		if row[8] != "" {
			team.EstablishedDate = row[8]
		}

		if row[9] != "" {
			team.DisbandedDate = row[9]
		}

		err = team.save(conn)

		if err != nil {
			return err
		}
	}
}

func insertTeam(conn *sql.DB, team Team) (sql.Result, error) {
	stmt, err := conn.Prepare(`
    INSERT INTO teams
    ( 
        id, name, short_name, abbreviation, website, established_date, disbanded_date, team_type_id, location, parent_league_id, genus_id, domain_id
    )
    VALUES
    (
        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
    )
    `)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	var domainID sql.NullInt64
	if team.Domain == nil {
		domainID.Valid = false
	} else {
		domainID.Valid = true
		domainID.Int64 = int64(team.Domain.ID)
	}

	return stmt.Exec(
		team.ID,
		team.Name,
		team.ShortName,
		team.Abbreviation,
		team.Website,
		team.EstablishedDate,
		team.DisbandedDate,
		team.TeamType.ID,
		team.Location,
		nil,
		team.Genus.ID,
		domainID)
}

func createTeamSchema(conn *sql.DB) error {
	stmt, err := conn.Prepare(`
    CREATE TABLE IF NOT EXISTS teams
    (
        id                INTEGER  PRIMARY KEY AUTOINCREMENT,
        name              TEXT     NOT NULL,
        short_name        TEXT     NOT NULL,
        abbreviation      TEXT,
        website           TEXT,
        established_date  TEXT,
        disbanded_date    TEXT,

        team_type_id      INTEGER,
        location,
        parent_league_id  INTEGER,
        genus_id          INTEGER,
        domain_id         INTEGER
    )
    `)
	defer stmt.Close()
	if err != nil {
		return err
	}

	_, err = stmt.Exec()
	return err
}
