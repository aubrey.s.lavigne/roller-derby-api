package db

import "database/sql"

type idSetter interface {
	SetID(int64)
}

type dbInserter interface {
	idSetter
	Insert(conn *sql.DB) (sql.Result, error)
}

// Save inserts the record into the appropriate table
func Save(crud dbInserter, conn *sql.DB) error {
	res, err := crud.Insert(conn)
	if err != nil {
		return err
	}

	id64, err := res.LastInsertId()
	crud.SetID(id64)
	return err
}
