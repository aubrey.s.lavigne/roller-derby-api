package models

import (
	"database/sql"
	"strings"
	"testing"

	"gitlab.com/aubrey.s.lavigne/roller-derby-api/db"

	_ "github.com/mattn/go-sqlite3"
)

func TestImportTeamCSV(t *testing.T) {

	conn, err := sql.Open("sqlite3", ":memory:?_foreign_keys=on")
	defer conn.Close()
	if err != nil {
		t.Fatalf("Could not open in-memory Sqlite3 connection: %q", err)
	}

	// Set up test database
	err = MigrateDB(conn)
	if err != nil {
		t.Fatalf("Migration Failure: %v", err)
	}

	input := `id,name,shortName,abbreviation,teamType,location,website,parentLeague,establishedDate,disbandedDate,genus,domain
"91229"," Deskarriados Roller Derby","Deskarriados","DRD","Travel Team","Osorno, Los Lagos, Chile","https://www.facebook.com/pg/losdeskarriados.rollerderby/","","2015-06-01","","Mens",""
"63224"," Easo Avengers Roller Derby"," Easo Avengers","EASO","Travel Team","Donostia-San Sebastián, Spain","https://www.facebook.com/profile.php?id=100008131933989&sk=about","","","","Womens","Europe"
`

	// Import the sample team test file
	err = ImportTeamCSV(conn, strings.NewReader(input))
	if err != nil {
		t.Errorf("Import Failed: %q", err)
	}

	runTeamTest(t, conn)
}

func runTeamTest(t *testing.T, conn *sql.DB) {
	// Evaluate if the import matches what's expected
	expectedNumTeams := 2
	if numTeams, err := db.CountTable(conn, "teams"); err != nil || numTeams != expectedNumTeams {
		t.Errorf("Unexpected number of teams in database. Got %d, expected %d", numTeams, expectedNumTeams)
	}

	tests := []struct {
		name          string
		ID            int
		expectedFound bool
		expectedTeam  Team
	}{
		{
			name:          "Team Not Found",
			ID:            123,
			expectedFound: false,
			expectedTeam:  Team{},
		},
		{
			name:          "Deskarriados Roller Derby",
			ID:            91229,
			expectedFound: true,
			expectedTeam: Team{
				ID:           91229,
				Name:         "Deskarriados Roller Derby",
				ShortName:    "Deskarriados",
				Abbreviation: "DRD",
				Website:      "https://www.facebook.com/pg/losdeskarriados.rollerderby/",
				TeamType: &TeamType{
					Name: "Travel Team",
				},
				Location:        "Osorno, Los Lagos, Chile",
				ParentLeague:    nil,
				EstablishedDate: "2015-06-01",
				Genus: &Genus{
					Name: "Mens",
				},
				Domain: nil,
			},
		},
		{
			name:          "Easo Avengers Roller Derby",
			ID:            63224,
			expectedFound: true,
			expectedTeam: Team{
				ID:           63224,
				Name:         "Easo Avengers Roller Derby",
				ShortName:    "Easo Avengers",
				Abbreviation: "EASO",
				Website:      "https://www.facebook.com/profile.php?id=100008131933989&sk=about",
				TeamType: &TeamType{
					Name: "Travel Team",
				},
				Location:        "Donostia-San Sebastián, Spain",
				ParentLeague:    nil,
				EstablishedDate: "",
				Genus: &Genus{
					Name: "Womens",
				},
				Domain: &Domain{
					Name: "Europe",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var team Team
			err := team.LoadID(conn, test.ID)

			if err != nil {
				t.Fatalf("Error occurred in Team lookup: %q", err)
			}

			if team.Found != test.expectedFound {
				t.Errorf("Lookup result expected. Got found: %t, expected: %t", team.Found, test.expectedFound)
			}

			if team.Found && test.expectedFound {
				assertTeam(t, team, test.expectedTeam)
			}
		})
	}
}

func assertTeam(t *testing.T, team, expected Team) {

	// Integer tests
	intTests := []struct {
		title         string
		value         int
		expectedValue int
	}{
		{title: "Team IDs", value: team.ID, expectedValue: expected.ID},
	}

	for _, test := range intTests {
		if test.value != test.expectedValue {
			t.Errorf("%s do not match. Got %d, expected: %d", test.title, test.value, test.expectedValue)
		}
	}

	// String tests
	stringTests := []struct {
		title         string
		value         string
		expectedValue string
	}{
		{title: "Team Names", value: team.Name, expectedValue: expected.Name},
		{title: "Team Short Names", value: team.ShortName, expectedValue: expected.ShortName},
		{title: "Team Abbreviation", value: team.Abbreviation, expectedValue: expected.Abbreviation},
		{title: "Team Website", value: team.Website, expectedValue: expected.Website},
		{title: "Team Location", value: team.Location, expectedValue: expected.Location},
		{title: "Team EstablishedDate", value: team.EstablishedDate, expectedValue: expected.EstablishedDate},
		{title: "Team DisbandedDate", value: team.DisbandedDate, expectedValue: expected.DisbandedDate},
	}

	for _, test := range stringTests {
		t.Run(test.title, func(t *testing.T) {
			if test.value != test.expectedValue {
				t.Errorf("%s do not match. Got %s, expected: %s", test.title, test.value, test.expectedValue)
			}
		})
	}

	// Team Type foreign key
	if team.TeamType.Name != expected.TeamType.Name {
		t.Errorf("Team TeamTypes do not match. Got %s, expected: %s", team.TeamType.Name, expected.TeamType.Name)
	}

	// Parent League foreign key
	teamParentLeagueID := -1
	if team.ParentLeague != nil {
		teamParentLeagueID = team.ParentLeague.ID
	}
	expectedParentLeagueID := -1
	if expected.ParentLeague != nil {
		expectedParentLeagueID = expected.ParentLeague.ID
	}
	if teamParentLeagueID != expectedParentLeagueID {
		t.Errorf("Team ParentLeagues do not match. Got %d, expected: %d", teamParentLeagueID, expectedParentLeagueID)
	}

	// Genus foreign key
	if team.Genus.Name != expected.Genus.Name {
		t.Errorf("Team Genera do not match. Got %s, expected: %s", team.Genus.Name, expected.Genus.Name)
	}

	// Domain foreign key
	teamDomainName := "Nil/Not Set"
	if team.Domain != nil {
		teamDomainName = team.Domain.Name
	}
	expectedDomainName := "Nil/Not Set"
	if expected.Domain != nil {
		expectedDomainName = expected.Domain.Name
	}
	if teamDomainName != expectedDomainName {
		t.Errorf("Team Domains do not match. Got %s, expected: %s", teamDomainName, expectedDomainName)
	}
}

func TestDuplicateMigrations(t *testing.T) {
	conn, err := sql.Open("sqlite3", ":memory:?_foreign_keys=on")
	defer conn.Close()
	if err != nil {
		t.Fatalf("Could not open in-memory Sqlite3 connection: %q", err)
	}

	// Set up test database
	err = MigrateDB(conn)
	if err != nil {
		t.Fatalf("Migration Failure on first pass: %v", err)
	}

	// Run it again -- this should not run into any issues
	err = MigrateDB(conn)
	if err != nil {
		t.Fatalf("Migration Failure on second pass: %v", err)
	}

	tests := []struct {
		tableName     string
		expectedCount int
	}{
		{tableName: "teams", expectedCount: 0},
		{tableName: "domains", expectedCount: 6},
		{tableName: "genera", expectedCount: 4},
		{tableName: "team_types", expectedCount: 4},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			if count, err := db.CountTable(conn, test.tableName); err != nil || count != test.expectedCount {
				t.Errorf("Unexpected number of %s in database. Got %d, expected %d", test.tableName, count, test.expectedCount)
			}
		})
	}
}

func TestDuplicateImportTeamCSV(t *testing.T) {

	conn, err := sql.Open("sqlite3", ":memory:?_foreign_keys=on")
	defer conn.Close()
	if err != nil {
		t.Fatalf("Could not open in-memory Sqlite3 connection: %q", err)
	}

	// Set up test database
	err = MigrateDB(conn)
	if err != nil {
		t.Fatalf("Migration Failure: %v", err)
	}

	input := `id,name,shortName,abbreviation,teamType,location,website,parentLeague,establishedDate,disbandedDate,genus,domain
"91229"," Deskarriados Roller Derby","Deskarriados","DRD","Travel Team","Osorno, Los Lagos, Chile","https://www.facebook.com/pg/losdeskarriados.rollerderby/","","2015-06-01","","Mens",""
"63224"," Easo Avengers Roller Derby"," Easo Avengers","EASO","Travel Team","Donostia-San Sebastián, Spain","https://www.facebook.com/profile.php?id=100008131933989&sk=about","","","","Womens","Europe"
`

	// Import the sample team test file
	err = ImportTeamCSV(conn, strings.NewReader(input))
	if err != nil {
		t.Errorf("Import Failed: %q", err)
	}

	// Import the sample team AGAIN!
	err = ImportTeamCSV(conn, strings.NewReader(input))
	if err != nil {
		t.Errorf("Import Failed: %q", err)
	}

	// Result should be the same as above
	runTeamTest(t, conn)
}
