package models

import (
	"database/sql"

	"gitlab.com/aubrey.s.lavigne/roller-derby-api/db"
)

// Genus represents who is playing this particular version of Derby
//
// For example: Mens, Womens, etc.
type Genus struct {
	Found bool `json:"-"`

	ID   int    `json:"id" field:"id,primarykey"`
	Name string `json:"name" field:"name"`
}

func (g *Genus) save(conn *sql.DB) error {
	return db.Save(g, conn)
}

// SetID sets the ID property for the Genus
func (g *Genus) SetID(id int64) {
	g.ID = int(id)
}

// Insert inserts the Genus into the genera table
func (g *Genus) Insert(conn *sql.DB) (sql.Result, error) {
	return insertGenus(conn, *g)
}

func createGenusSchema(conn *sql.DB) error {
	stmt, err := conn.Prepare(`
    CREATE TABLE IF NOT EXISTS genera
    (
       id   INTEGER PRIMARY KEY,
       name TEXT    NOT NULL
    )
    `)
	defer stmt.Close()
	if err != nil {
		return err
	}

	_, err = stmt.Exec()
	return err
}

func seedGenus(conn *sql.DB) error {
	genera := []string{
		"Coed",
		"Junior",
		"Mens",
		"Womens",
	}

	for _, genus := range genera {
		obj, err := loadGenusByName(conn, genus)
		if err != nil {
			return err
		}

		if !obj.Found {
			obj.Name = genus
			err = obj.save(conn)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func loadGenusByName(conn *sql.DB, genusName string) (Genus, error) {
	var res Genus

	err := conn.QueryRow(`
    SELECT id, name
    FROM genera
    WHERE name = ?
    `, genusName).Scan(&res.ID, &res.Name)

	res.Found = (err == nil)
	if err == sql.ErrNoRows {
		err = nil
	}

	return res, err
}

func insertGenus(conn *sql.DB, genus Genus) (sql.Result, error) {
	stmt, err := conn.Prepare(`
    INSERT INTO genera (name)
    VALUES (?)
    `)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	return stmt.Exec(genus.Name)
}

// LoadAllGenera returns all available Genera
func LoadAllGenera(conn *sql.DB) ([]Genus, error) {
	res := []Genus{}

	rows, err := conn.Query(`
    SELECT id, name
    FROM genera
    `)
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var d Genus
		err := rows.Scan(&d.ID, &d.Name)
		if err != nil {
			return res, err
		}
		res = append(res, d)
	}

	return res, err
}

func generaLookupMap(conn *sql.DB) (map[string]int, error) {
	return db.TableLookupMap(conn, "genera", "id", "name")
}
