package models

import (
	"database/sql"
	"testing"
)

func TestInsertAndLoadGenus(t *testing.T) {
	conn, err := sql.Open("sqlite3", ":memory:?_foreign_keys=on")
	defer conn.Close()
	if err != nil {
		t.Fatalf("Could not open in-memory Sqlite3 connection: %q", err)
	}

	// Set up test database
	err = createSchema(conn)
	if err != nil {
		t.Errorf("Could not create database schema: %q", err)
	}

	// Check that the genus starts empty
	emptyGenus, err := loadGenusByName(conn, "Womens")
	if emptyGenus.Found {
		t.Errorf("Empty genus should not be found")
	}

	// Insert a single genus
	savingGenus := Genus{
		Name: "Womens",
	}
	err = savingGenus.save(conn)
	if err != nil {
		t.Fatalf("Should not error while saving genus")
	}

	loadedGenus, err := loadGenusByName(conn, "Womens")
	if !loadedGenus.Found {
		t.Errorf("Loaded Genus should be found")
	}
	if id := loadedGenus.ID; id != 1 {
		t.Errorf("Unexpected loaded Genus ID. Got %d, expected %d", id, 1)
	}
	if name := loadedGenus.Name; name != "Womens" {
		t.Errorf("Unexpected loaded Genus name. Got %s, expected %s", name, "Womens")
	}
}
