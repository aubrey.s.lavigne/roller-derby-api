package db

import (
	"database/sql"
	"fmt"
)

// CountTable returns the number of rows in the target table
func CountTable(conn *sql.DB, tableName string) (int, error) {

	query := fmt.Sprintf(`
    SELECT COUNT(id) as num
    FROM %s
    `, tableName)

	var num int
	err := conn.QueryRow(query).Scan(&num)

	return num, err
}

// TableLookupMap returns a map from the value name to the corresponding
// primary key value
func TableLookupMap(conn *sql.DB, tableName, keyName, valueName string) (map[string]int, error) {
	query := fmt.Sprintf(`
        SELECT %s, %s
        FROM %s
    `, keyName, valueName, tableName)

	rows, err := conn.Query(query)
	defer rows.Close()

	res := make(map[string]int)
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var id int
		var name string
		err := rows.Scan(&id, &name)
		if err != nil {
			return res, err
		}
		res[name] = id
	}

	return res, nil
}
