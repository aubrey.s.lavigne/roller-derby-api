package db

import (
	"reflect"
	"strings"
)

type dbField struct {
	f reflect.StructField
}

func (f dbField) tags() []string {
	fieldTag := f.f.Tag.Get("field")
	return strings.Split(fieldTag, ",")
}

func (f dbField) isPrimaryKey() bool {
	for _, tag := range f.tags() {
		if tag == "primarykey" {
			return true
		}
	}
	return false
}

func (f dbField) isDBField() bool {
	return f.f.Tag.Get("field") != ""
}

func (f dbField) DBName() string {

	for _, tag := range f.tags() {
		if tag != "primarykey" {
			return tag
		}
	}
	return ""
}

func (f dbField) getType() string {
	switch f.f.Type.Kind() {

	case reflect.Bool:
		fallthrough
	case reflect.Int:
		fallthrough
	case reflect.Int64:
		return "INTEGER"

	case reflect.Float64:
		return "REAL"

	case reflect.String:
		return "TEXT"

	default:
		return ""
	}
}
