package db

import (
	"reflect"
	"testing"
)

// basicTypesStruct is a sample structure to use with field testing
type BasicTypesStruct struct {
	blankField int
	intField   int     `field:"int_field"`
	strField   string  `field:"str_field,primarykey"`
	floatField float64 `field:"floating_point_field"`
}

func TestDBField(t *testing.T) {
	type expected struct {
		isPrimaryKey bool
		isDBField    bool
		name         string
		fieldType    string
	}
	tests := []struct {
		fieldNum int
		expected expected
	}{
		{
			fieldNum: 0,
			expected: expected{
				isPrimaryKey: false,
				isDBField:    false,
				name:         "",
				fieldType:    "INTEGER",
			},
		},
		{
			fieldNum: 1,
			expected: expected{
				isPrimaryKey: false,
				isDBField:    true,
				name:         "int_field",
				fieldType:    "INTEGER",
			},
		},
		{
			fieldNum: 2,
			expected: expected{
				isPrimaryKey: true,
				isDBField:    true,
				name:         "str_field",
				fieldType:    "TEXT",
			},
		},
		{
			fieldNum: 3,
			expected: expected{
				isPrimaryKey: false,
				isDBField:    true,
				name:         "floating_point_field",
				fieldType:    "REAL",
			},
		},
	}

	st := reflect.TypeOf(BasicTypesStruct{})
	for _, test := range tests {
		field := dbField{
			f: st.Field(test.fieldNum),
		}

		if res := field.isPrimaryKey(); res != test.expected.isPrimaryKey {
			t.Errorf("unexpected isPrimaryKey() lookup. Got %t, expected %t", res, test.expected.isPrimaryKey)
		}

		if res := field.isDBField(); res != test.expected.isDBField {
			t.Errorf("unexpected isDBField() lookup. Got %t, expected %t", res, test.expected.isDBField)
		}

		if res := field.DBName(); res != test.expected.name {
			t.Errorf("unexpected DBName() lookup. Got %s, expected %s", res, test.expected.name)
		}

		if res := field.getType(); res != test.expected.fieldType {
			t.Errorf("unexpected getType() lookup. Got %s, expected %s", res, test.expected.fieldType)
		}

	}
}
