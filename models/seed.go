package models

import (
	"database/sql"
	"fmt"
)

// MigrateDB creates the initial database schema and seeds it with the
// default expected values
func MigrateDB(conn *sql.DB) error {
	// Set up test database
	err := createSchema(conn)
	if err != nil {
		return fmt.Errorf("Could not create database schema: %q", err)
	}

	err = seedData(conn)
	if err != nil {
		return fmt.Errorf("Could not seed database database: %q", err)
	}

	return nil
}

func createSchema(conn *sql.DB) error {

	err := createTeamTypeSchema(conn)
	if err != nil {
		return err
	}

	err = createGenusSchema(conn)
	if err != nil {
		return err
	}

	err = createDomainSchema(conn)
	if err != nil {
		return err
	}

	err = createTeamSchema(conn)
	if err != nil {
		return err
	}

	return nil
}

func seedData(conn *sql.DB) error {

	err := seedDomain(conn)
	if err != nil {
		return err
	}

	err = seedGenus(conn)
	if err != nil {
		return err
	}

	err = seedTeamType(conn)
	if err != nil {
		return err
	}

	return nil
}
